from nltk.tokenize import word_tokenize
import psycopg2
from lxml import html
from bs4 import BeautifulSoup, Comment
import re
import time
import sys
import glob

import dbfunctions
from stopwords import stop_words_slovene

def clean_HTML(html):
    attr_whitelist = {
        'a': ['href', 'title', 'hreflang'],
    }
    tag_blacklist = ['script', 'head', 'style', 'img', 'input', 'form', 'figure', 'iframe', 'footer', 'link', 'meta', 'select',
                     'option', 'onl-video-banner', 'onl-magick-box', 'onl-header']
    soup = BeautifulSoup(html, features="lxml")

    for comment in soup.findAll(
            text=lambda text: isinstance(text, Comment)):
        comment.extract()

    for tag in soup.findAll():
        # Odstrani tag
        if tag.name.lower() in tag_blacklist:
            tag.extract()
        else:
            for attr in tag.attrs.copy():
                if not (tag.name.lower() in attr_whitelist and attr[0].lower() in attr_whitelist[tag.name.lower()]):
                    del tag.attrs[attr]

    for x in soup.find_all():
        if len(x.get_text(strip=True)) == 0:
            x.extract()
    # zapis v test_izpis
    clean_html = soup.prettify()
    clean_html = re.sub("-", "—", clean_html)
    return clean_html

def getHtml(location):
    pageContent = open(location, 'rb').read()
    tree = html.fromstring(pageContent)
    clean = clean_HTML(pageContent)
    return clean

def processHtml(html):
    soup = BeautifulSoup(html, 'html.parser')

    # kill all script and style elements
    for script in soup(["script", "style"]):
        script.extract()    # rip it out

    # get text
    text = soup.get_text()
    # break into lines and remove leading and trailing space on each
    lines = (line.strip() for line in text.splitlines())
    # break multi-headlines into a line each
    chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
    # drop blank lines
    text = '\n'.join(chunk for chunk in chunks if chunk)
    # tokenize text with nltk library
    tokenized = word_tokenize(text)
    # remove stopwords
    #filtered_sentence = [(w.lower(), i) for i, w in enumerate(tokenized) if not w in stop_words_slovene]

    return tokenized

# get start time for execution time of the program
startTime = time.time()

tokenized = sys.argv[1:]
filtered_search = [w.lower() for w in tokenized if not w in stop_words_slovene]


results = {}
for filepath in glob.iglob('./data/*/*.html'):
    # get html from file
    soup = getHtml(filepath)
    # tokenization, removing stopwords, lowercase...
    tokenized = processHtml(soup)
    filtered_sentence = [(w.lower(), i) for i, w in enumerate(tokenized) if not w in stop_words_slovene]


    frequency = 0
    indexes = []
    for (w, i) in filtered_sentence:
        if w in filtered_search:
            frequency += 1
            # check that same snippet doesn't apper more than once
            notInInd = True
            for j in indexes:
                if abs(i - j) < 3:
                    notInInd = False
            if notInInd:
                indexes.append(i)

    if frequency > 0:
        # get snippet from indexes array
        # get document to extract snippet
        snippet = ""
        for ind in indexes:
            start = max(0, ind - 3)
            end = ind + 4
            while start < end and start < len(tokenized):
                snippet += tokenized[start] + " "
                start += 1
            snippet += "... "
        # save freqency and snippet to results
        results[filepath] = {'f' : frequency, 's' : snippet[0:min(len(snippet)-1, 250)]}

from collections import OrderedDict
od = OrderedDict(sorted(results.items(), key=lambda x: x[1]['f'], reverse=True))

print("Results for a query: '" + str(tokenized).strip('[]').replace(",", "").replace("'", "") + "'\n")
print("Results found in " + str(round((time.time() - startTime), 3)) + "s\n")
print("Frequencies  Document                Snippet")
print("-----------  ----------------------  -------------------------------------------------------------")
for doc in od:
    print(str(od[doc]['f']) + "            " + doc.split("/")[-1] + "   " + od[doc]['s'])

print("Število rezultatov: " + str(len(od.keys())))
