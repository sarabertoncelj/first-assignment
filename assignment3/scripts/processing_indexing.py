from bs4 import BeautifulSoup, Comment
from lxml import html
import re
import psycopg2
from nltk.tokenize import word_tokenize
import glob

from stopwords import stop_words_slovene
import dbfunctions

def clean_HTML(html):
    attr_whitelist = {
        'a': ['href', 'title', 'hreflang'],
    }
    tag_blacklist = ['script', 'head', 'style', 'img', 'input', 'form', 'figure', 'iframe', 'footer', 'link', 'meta', 'select',
                     'option', 'onl-video-banner', 'onl-magick-box', 'onl-header']
    soup = BeautifulSoup(html, features="lxml")

    for comment in soup.findAll(
            text=lambda text: isinstance(text, Comment)):
        comment.extract()

    for tag in soup.findAll():
        # Odstrani tag
        if tag.name.lower() in tag_blacklist:
            tag.extract()
        else:
            for attr in tag.attrs.copy():
                if not (tag.name.lower() in attr_whitelist and attr[0].lower() in attr_whitelist[tag.name.lower()]):
                    del tag.attrs[attr]

    for x in soup.find_all():
        if len(x.get_text(strip=True)) == 0:
            x.extract()
    # zapis v test_izpis
    clean_html = soup.prettify()
    clean_html = re.sub("-", "—", clean_html)
    return clean_html

def getHtml(location):
    pageContent = open(location, 'rb').read()
    tree = html.fromstring(pageContent)
    clean = clean_HTML(pageContent)
    return clean

def processHtml(html):
    soup = BeautifulSoup(html, 'html.parser')

    # kill all script and style elements
    for script in soup(["script", "style"]):
        script.extract()    # rip it out

    # get text
    text = soup.get_text()
    # break into lines and remove leading and trailing space on each
    lines = (line.strip() for line in text.splitlines())
    # break multi-headlines into a line each
    chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
    # drop blank lines
    text = '\n'.join(chunk for chunk in chunks if chunk)
    # tokenize text with nltk library
    tokenized = word_tokenize(text)
    # remove stopwords
    filtered_sentence = [(w.lower(), i) for i, w in enumerate(tokenized) if not w in stop_words_slovene]

    return filtered_sentence

def wordFrequencies(filtered_sentence):
    dict = {}
    for (w, i) in filtered_sentence:
        if w in dict.keys():
            dict[w]['f'] = dict[w]['f'] + 1
            indexes = dict[w]['i']
            indexes.append(i)
        else:
            dict[w] = {'f': 1, 'i': [i]}

    return dict

def addToDb(conn, dict, filepath):

    for w in dict:
        word = w.strip()
        # check if word already in IndexWord table, if not add
        if not dbfunctions.get_word(conn, w):
            #print("******************** create word " + str(w))
            dbfunctions.create_word(conn, w)
        documentName = filepath
        frequency = dict[w]['f']
        indexes = str(dict[w]['i'])[1:-1]
        #print("create posting " + str(w))
        dbfunctions.create_posting(conn, word, documentName, frequency, indexes)


dbfunctions.clear_db()

conn = psycopg2.connect(host="localhost", user="user", password="SecretPassword")
conn.autocommit = True

for filepath in glob.iglob('./data/*/*.html'):
    print(filepath)
    # get html from file
    soup = getHtml(filepath)
    # tokenization, removing stopwords, lowercase...
    filtered_sentence = processHtml(soup)
    # calculate frequencies
    dict = wordFrequencies(filtered_sentence)
    # add to database
    addToDb(conn, dict, filepath)

conn.close()
