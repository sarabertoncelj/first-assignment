import psycopg2

# delete all entries from database
def clear_db():
    conn = psycopg2.connect(host="localhost", user="user", password="SecretPassword")
    conn.autocommit = True

    cur = conn.cursor()

    allTablesStr = "IndexWord, Posting"
    # delete all entries in all tables
    cur.execute("TRUNCATE " + allTablesStr)

    cur.close()
    conn.close()

# create new word entry
def create_word(conn, word):
    cur = conn.cursor()
    #print(domain)
    query = f"INSERT INTO IndexWord VALUES (%s)"
    cur.execute(query, (word, ))

    cur.close()

# create new posting entry
def create_posting(conn, word, documentName, frequency, indexes):

    cur = conn.cursor()
    query = f"INSERT INTO Posting VALUES (%s, %s, %s, %s)"
    cur.execute(query, (word, documentName, frequency, indexes))

    cur.close()

# check if word already exists in database, if does return word else return none
def get_word(conn, word):

    cur = conn.cursor()
    query = f"SELECT word FROM IndexWord WHERE  word = %s"
    cur.execute(query, (word, ))
    res = cur.fetchone()
    cur.close()
    if res:
        return res[0]
    else:
        return None

# get all from posting where word matches
def get_posting(conn, words):
    query = "SELECT * FROM Posting WHERE"
    for word in words:
        query += " word = '" + word + "' OR"
    query = query[:-3]

    cur = conn.cursor()
    cur.execute(query)
    res = cur.fetchall()
    cur.close()
    return res
