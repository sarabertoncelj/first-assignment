# Knjižnice potrebne za izvajanje programov
Knjižnice, ki so potrebne za zagon skript so:
- nltk.tokenize
- psycopg2
- lxml
- bs4
- re
- glob

# Opis programov in navodila za zagon
Poti do html datotek s podatki so napisane relativno tako, da se python programe kliče iz mape assignment3 (npr. python scripts/run-sqlite-search.py).

Datoteka processing_indexing.py vsebuje skripto uporabljeno za izgradnjo baze.
Datoteka dbfunctions vsebuje funkcije, ki izvedejo sql poizvedbe in se kličejo iz skript processing_indexing.py in run-sqlite-search.py.
Datoteki run-basic-search.py in run-sqlite-search.py se kličeta z dodatnimi argumenti, ki predstavljajo poizvedbo in vrneta rezultate za to poizvedbo iz baze oz. neposredno iz datotek.
Datoteka stopwords.py vsebuje besede, ki se izločijo kot nerelavantne iz html datotek med procesiranjem.
