# We rather use the locally-cached file as it may have changed online.
import requests
from lxml import html
import json
import re

webPages_rtv = ['../input-extraction/WebPages/rtvslo.si/Volvo XC 40 D4 AWD momentum_ suvereno med najboljse v razredu - RTVSLO.si.html', '../input-extraction/WebPages/rtvslo.si/Audi A6 50 TDI quattro_ nemir v premijskem razredu - RTVSLO.si.html']
webPages_overstock = ['../input-extraction/WebPages/overstock.com/jewelry01.html','../input-extraction/WebPages/overstock.com/jewelry02.html' ]
webPages_okusno = ['../input-extraction/WebPages/okusno.je/Paprike - Okusno.je.html', '../input-extraction/WebPages/okusno.je/Sladice - Okusno.je.html']

for i in webPages_rtv:
    pageContent = open(i, 'r', encoding='utf-8').read()
    # print("Page content:\n'%s'." % pageContent)
    # Get the article title
    regex = r'<h1>(.*)</h1>'
    match = re.compile(regex).search(pageContent)
    title = match.group(1)
    # print("Found title: '%s'." % title)

    # Get the article author
    regex = r"<div class=\"author-name\">(.*)</div>"
    match = re.compile(regex).search(pageContent)
    author = "not found"
    if match:
        author = match.group(1)
    # print("Found date: '%s'." % author)

    # Get the article date
    regex = r"([0-3]?[0-9]\.\s(januar|februar|marec|april|maj|junij|julij|avgust|september|oktober|november|december)\s[12][09][01289][0-9])"
    match = re.compile(regex).search(pageContent)
    publishedTime = "not found"
    if match:
        publishedTime = match.group(1)
    # print("Found date: '%s'." % publishedTime)

    # Get the article subtitle
    regex = r"<div class=\"subtitle\">(.*)</div>"
    match = re.compile(regex).search(pageContent)
    subtitle = "not found"
    if match:
        subtitle = match.group(1)
    # print("Found date: '%s'." % subtitle)

    # Get the article lead
    regex = r"<p class=\"lead\">(.*)"
    match = re.compile(regex).search(pageContent)
    lead = "not found"
    if match:
        lead = match.group(1)
    # print("Found lead: '%s'." % lead)

    # Get the article content
    regex = r"<div class=\"article-body\">((.|\n)*)<\/article>"
    match = re.compile(regex).search(pageContent)
    content = "not found"
    if match:
        content = match.group(1)
    clean_content = re.sub("<script[^<>]*>((\n|.)*)<\/script>|<([^<>]*)>|(\t|\n)*", "", content)
    # print("Found content: '%s'." % clean_content)

    # Format json
    dataItem = {
        "Title": title,
        "Author": author,
        "PublishedTime": publishedTime,
        "SubTitle": subtitle,
        "Lead": lead,
        "Content": clean_content
    }

    print("Output object:\n%s" % json.dumps(dataItem, indent = 4, ensure_ascii=False))

for i in webPages_overstock:
    dataList = []
    pageContent = open(i, 'r').read()
    # print("Page content:\n'%s'." % pageContent)
    # Get the title
    regex = r'<a href="http:\/\/www\.overstock\.com\/cgi-bin\/d2\.cgi\?PAGE=PROFRAME&amp;PROD_ID=[0-9]*"><b>((.|\n)*?)<\/b>'
    titles = re.compile(regex).findall(pageContent)
    for m in titles:
        title = m[0]
     #   print("Found title: '%s'." % title)

    # Get the listPrice
    regex = r"<b>List\s*Price[^$]*([^<]*)"
    listPrices = re.compile(regex).findall(pageContent)
    listPrice = "not found"
    for m in listPrices:
        listPrice = m
    #    print("Found list price: '%s'." % listPrice)

    # Get the price
    regex = r"<b>Price[^$]*([^<]*)"
    prices = re.compile(regex).findall(pageContent)
    #print("Found date: '%s'." % prices)

    # Get the saving
    regex = r"<b>You Save:[^$]*([^ ]*)"
    savings = re.compile(regex).findall(pageContent)
    # print("Found date: '%s'." % subtitle)

    # Get the savingPercent
    regex = r"<b>You Save:[^$]*[^(]*\(([^)]*)"
    savingPercents = re.compile(regex).findall(pageContent)
    # print("Found date: '%s'." % lead)

    # Get the content
    regex = r"<td valign=\"top\"><span class=\"normal\">((.|\n)*?)<\/td><\/tr><\/tbody><\/table>"
    contents = re.compile(regex).findall(pageContent)
    clean_contents = []
    for content in contents:
        clean_contents.append(re.sub("<script[^<>]*>((\n|.)*)<\/script>|<([^<>]*)>|(\t|\n)*", "", content[0]))
    # print("Found content: ", clean_contents, ".")

    # Format json
    for i, title in enumerate(titles):
        dataItem = {
            "Title": title[0],
            "ListPrice": listPrices[i],
            "Price": prices[i],
            "Savings": savings[i],
            "SavingPercent": savingPercents[i],
            "Content": clean_contents[i]
        }
        dataList.append(dataItem)
    print("Output object:\n%s" % json.dumps(dataList, indent=4, ensure_ascii=False))

for i in webPages_okusno:
    pageContent = open(i, 'r', encoding='utf-8').read()
    regex = r'<h1 class="font-bold text-secondary text-18 md:text-28 leading-normal pt-0 p-16 md:pb-0 md:p-32 pb-0 bg-white rounded-8-t">(.*)<\/h1>'
    match = re.compile(regex).search(pageContent)
    title = match.group(1)
    # print("Found title: '%s'." % title)

    # Get the title
    regex = r'<div class="text-secondary font-semi mt-16"><p.*?>(.*)<\/p>'
    match = re.compile(regex).search(pageContent)
    subtitle = "not found"
    if match:
        subtitle = match.group(1)
    # print("Found subtitle: '%s'." % subtitle)

    # Get the subtitle
    regex = r'<div class="label recipe">(.*?)<\/div>'
    match = re.compile(regex).search(pageContent)
    foodType = "not found"
    if match:
        foodType = match.group(1).lower()
    # print("Found food type: '%s'." % foodType)

    # Get the instructions
    regex = r'<div class="md:border border-grey-light rounded-8-b preperation-box overflow-hidden relative timeline-preperation">(.*?)<div class="tips-and-sides--box.*?">'
    match = re.compile(regex).search(pageContent)
    recipe = "not found"
    if match:
        recipe = match.group(1)
    clean_recipe = re.sub("<script[^<>]*>((\n|.)*)<\/script>|<([^<>]*)>|(\t|\n|&nbsp;)*", "", recipe)
    # print("Found recipe: '%s'." % clean_recipe)

    # Get the preparation time
    regex = r'PRIPRAVA<\/span>\s*(.*?)<\/div>'
    match = re.compile(regex).search(pageContent)
    preparationTime = "not found"
    if match:
        preparationTime = match.group(1).lower()
    # print("Found preparation time: '%s'." % preparationTime)

    # Get the cooking time
    regex = r'KUHANJE<\/span>\s*(.*?)<\/div>'
    match = re.compile(regex).search(pageContent)
    cookingTime = "not found"
    if match:
        cookingTime = match.group(1).lower()
    # print("Found cooking time: '%s'." % cookingTime)

    # Get the ingredients
    regex = r'<div class="w-2\/3 md:4\/5 lg:w-2\/3 p-8 leading-normal">\s*(.*?)<\/div>'
    ingredients_all = re.compile(regex).findall(pageContent)
    regex = r'<div class="ingredient ng-tns-c1-[0-9] ng-star-inserted"><div class="border border-l-0 border-r-0 md:border-l md:border-r border-grey-light border-t-0 flex text-secondary transition hover:bg-black02 ingredient--item">(.*?)<\/div>(.*?)<\/div>(.*?)<\/div><\/div>'
    ingredients_all = re.compile(regex).findall(pageContent)
    amounts = []
    ingredients = []
    for i in ingredients_all:
        amounts.append(re.sub("<script[^<>]*>((\n|.)*)<\/script>|<([^<>]*)>|(\t|\n|&nbsp;)*", "", i[0]) + " " + re.sub("<script[^<>]*>((\n|.)*)<\/script>|<([^<>]*)>|(\t|\n|&nbsp;)*", "", i[1]))
        ingredients.append(re.sub("<script[^<>]*>((\n|.)*)<\/script>|<([^<>]*)>|(\t|\n|&nbsp;)*", "", i[2]))
    # print("Found ingredients: '%s'." % amounts)
    # print("Found ingredients: '%s'." % ingredients)

    # Format json
    ingredientList = []
    for i, ingredient in enumerate(ingredients):
        ingredientItem = {
            "Ingredient": ingredient,
            "Amount": amounts[i],
        }
        ingredientList.append(ingredientItem)
    dataItem = {
        "Title": title,
        "Subtitle": subtitle,
        "FoodType": foodType,
        "Recipe": clean_recipe,
        "PreperationTime": preparationTime,
        "CookingTime": cookingTime,
        "Ingredients": ingredientList,

    }
    print("Output object:\n%s" % json.dumps(dataItem, indent=4, ensure_ascii=False))