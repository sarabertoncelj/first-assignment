# We rather use the locally-cached file as it may have changed online.
from lxml import html
import json
import re

webPages_rtv = ['../input-extraction/WebPages/rtvslo.si/Volvo XC 40 D4 AWD momentum_ suvereno med najboljse v razredu - RTVSLO.si.html', '../input-extraction/WebPages/rtvslo.si/Audi A6 50 TDI quattro_ nemir v premijskem razredu - RTVSLO.si.html']
webPages_overstock = ['../input-extraction/WebPages/overstock.com/jewelry01.html','../input-extraction/WebPages/overstock.com/jewelry02.html' ]
webPages_okusno = ['../input-extraction/WebPages/okusno.je/Paprike - Okusno.je.html', '../input-extraction/WebPages/okusno.je/Sladice - Okusno.je.html']


for i in webPages_rtv: 
    pageContent = open(i, 'r', encoding='utf-8').read()
    tree = html.fromstring(pageContent)
    title = str(tree.xpath('//*[@id="main-container"]/div[3]/div/header/h1/text()')[0])
    author = str(tree.xpath('//*[@id="main-container"]/div[3]/div/header/div[3]/div[1]/strong/text()')[0])

    publishedTime = str((tree.xpath('//*[@id="main-container"]/div[3]/div/div[1]/div[2]/text()[1]'))[0])
    publishedTime = re.sub(r'(^[ \t]+|[ \t]+(?=:)+|^[ \n])', '', publishedTime, flags=re.M)

    subTitle = str(tree.xpath('//*[@id="main-container"]/div[3]/div/header/div[2]/text()')[0])
    lead = str(tree.xpath('//*[@id="main-container"]/div[3]/div/header/p/text()')[0])
    content = "".join((tree.xpath('//*[@id="main-container"]/div[3]/div/div[2]/article/child::p/text()')))

    dataItem = {
        "Title": title,
        "Author": author, 
        "PublishedTime": publishedTime,
        "SubTitle": subTitle, 
        "Lead": lead,
        "Content": content
    }
    print("Output object:\n%s" % json.dumps(dataItem, indent = 4, ensure_ascii=False))

for i in webPages_overstock: 
    pageContent = open(i, 'r').read()
    tree = html.fromstring(pageContent)
    dataList = []
    for i in range(50):
        try:
            title = str(tree.xpath('/html/body/table[2]/tbody/tr[1]/td[5]/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[%d]/td[2]/a/b/text()' %i)[0])
        except IndexError:
            title = None 
        if title is not None: 
            listPrice = str(tree.xpath('/html/body/table[2]/tbody/tr[1]/td[5]/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[%d]/td[2]/table/tbody/tr/td[1]/table/tbody/tr[1]/td[2]/s/text()' %i)[0])
            price = str(tree.xpath('/html/body/table[2]/tbody/tr[1]/td[5]/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[%d]/td[2]/table/tbody/tr/td[1]/table/tbody/tr[2]/td[2]/span/b/text()' % i)[0])
            savingAndSavingPercent = str(tree.xpath('/html/body/table[2]/tbody/tr[1]/td[5]/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[%d]/td[2]/table/tbody/tr/td[1]/table/tbody/tr[3]/td[2]/span/text()' % i)[0])
            saving, savingPercent = savingAndSavingPercent.split()
            content = str(tree.xpath('/html/body/table[2]/tbody/tr[1]/td[5]/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[%d]/td[2]/table/tbody/tr/td[2]/span/text()' % i)[0])
            content = content.replace("\n", " ")
            dataItem = {
                "Title": title,
                "ListPrice": listPrice, 
                "Price": price,
                "Savings": saving, 
                "SavingPercent": savingPercent,
                "Content": content
            }
            dataList.append(dataItem)
    print("Output object:\n%s" % json.dumps(dataList, indent=4, ensure_ascii=False))

            

for i in webPages_okusno: 
    pageContent = open(i, 'r', encoding='utf-8').read()
    tree = html.fromstring(pageContent)
    ingredient_values = []
    ingredient_names = []

    title = str(tree.xpath('/html/body/onl-root/div[1]/div[3]/div[2]/onl-recipe/div[2]/div/div[1]/div[1]/main/div/div[1]/div[2]/h1/text()')[0])
    subTitle = str(tree.xpath('/html/body/onl-root/div[1]/div[3]/div[2]/onl-recipe/div[2]/div/div[1]/div[1]/main/div/div[2]/div[1]/div/p/text()')[0])
    foodType = str(tree.xpath(' /html/body/onl-root/div[1]/div[3]/div[2]/onl-recipe/div[2]/div/div[1]/div[1]/main/div/div[1]/div[2]/div/div/text()')[0]).lower()
    reciepe =  "".join(tree.xpath('/html/body/onl-root/div[1]/div[3]/div[2]/onl-recipe/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[2]//child::p/text()'))
    preperationTime =  str(tree.xpath('/html/body/onl-root/div[1]/div[3]/div[2]/onl-recipe/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[1]/div[3]/div[1]/text()')[0]).strip()
    coockingTime =  str(tree.xpath('/html/body/onl-root/div[1]/div[3]/div[2]/onl-recipe/div[2]/div/div[1]/div[2]/div[1]/div[2]/div[1]/div[3]/div[2]/text()')[0]).strip()
    
    #ingredient = " ".join(tree.xpath('/html/body/onl-root/div[1]/div[3]/div[2]/onl-recipe/div[2]/div/div[1]/div[2]/div[1]/div[1]/div[3]/div/div[1]/child::div/text()' ))
    #/html/body/onl-root/div[1]/div[3]/div[2]/onl-recipe/div[2]/div/div[1]/div[2]/div[1]/div[1]/div[3]/div/div[1]
    for i in range (50):
        try:
            ingredient_value = " ".join(tree.xpath('/html/body/onl-root/div[1]/div[3]/div[2]/onl-recipe/div[2]/div/div[1]/div[2]/div[1]/div[1]/div[%d]/div/div[1]/child::div/text()'  % i))
            ingredient_name =  str(tree.xpath('/html/body/onl-root/div[1]/div[3]/div[2]/onl-recipe/div[2]/div/div[1]/div[2]/div[1]/div[1]/div[%d]/div/div[2]/text()' %i)[0])

         #   if (ingredient_name is not ""):
            ingredient_names.append(ingredient_name)

            if (ingredient_name is  ""):
                ingredient_names.append(None)
            ingredient_values.append(ingredient_value)

        except IndexError:
            title = None 
    ingredientList = []

    for i, ingredient in enumerate(ingredient_names):
        ingredientItem = {
            "Ingredient": ingredient,
            "Amount": ingredient_values[i],
        }
        ingredientList.append(ingredientItem)

    dataItem = {
        "Title": title,
        "Subtitle": subTitle,
        "FoodType": foodType,
        "Recipe": reciepe,
        "PreperationTime": preperationTime,
        "CookingTime": coockingTime,
        "Ingredients": ingredientList,
    }
    print("Output object:\n%s" % json.dumps(dataItem, indent=4, ensure_ascii=False))