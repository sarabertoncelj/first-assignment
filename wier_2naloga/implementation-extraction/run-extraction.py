import sys


def main():
    if str(sys.argv[1]) == 'A':
        import A
    elif str(sys.argv[1]) == 'B':
        import B
    elif str(sys.argv[1]) == 'C':
        import C
    else:
        print(str(sys.argv[1]))
        print("Vnesi pravilen parameter (A ali B ali C)")


if __name__ == "__main__":
    main()
