import requests
from lxml import html
import json
import re
from bs4 import BeautifulSoup, Comment
import difflib
import numpy as np

webPages_rtv = ['../input-extraction/WebPages/rtvslo.si/Volvo XC 40 D4 AWD momentum_ suvereno med najboljse v razredu - RTVSLO.si.html', '../input-extraction/WebPages/rtvslo.si/Audi A6 50 TDI quattro_ nemir v premijskem razredu - RTVSLO.si.html']
webPages_overstock = ['../input-extraction/WebPages/overstock.com/jewelry01.html','../input-extraction/WebPages/overstock.com/jewelry02.html' ]
webPages_okusno = ['../input-extraction/WebPages/okusno.je/Paprike - Okusno.je.html', '../input-extraction/WebPages/okusno.je/Sladice - Okusno.je.html']

#pageContent_1 = open('WebPages/rtvslo.si/Audi A6 50 TDI quattro_ nemir v premijskem razredu - RTVSLO.si.html', 'r', encoding='utf-8').read()
#pageContent_2 = open('WebPages/rtvslo.si/Audi A6 50 TDI quattro_ nemir v premijskem razredu - RTVSLO.si.html', 'r', encoding='utf-8').read()


def clean_HTML(html):
    attr_whitelist = {
        'a': ['href', 'title', 'hreflang'],
    }
    tag_blacklist = ['script', 'head', 'style', 'img', 'input', 'form', 'figure', 'iframe', 'footer', 'link', 'meta', 'select',
                     'option', 'onl-video-banner', 'onl-magick-box', 'onl-header']
    soup = BeautifulSoup(html, features="lxml")

    for comment in soup.findAll(
            text=lambda text: isinstance(text, Comment)):
        comment.extract()

    for tag in soup.findAll():
        # Odstrani tag
        if tag.name.lower() in tag_blacklist:
            tag.extract()
        else:
            for attr in tag.attrs.copy():
                # Če ni v whitelist removi
                if not (tag.name.lower() in attr_whitelist and attr[0].lower() in attr_whitelist[tag.name.lower()]):
                    del tag.attrs[attr]

    for x in soup.find_all():
        if len(x.get_text(strip=True)) == 0:
            x.extract()
    # zapis v test_izpis
    f = open("test_izpis.txt", "w", encoding="utf-8")
    clean_html = soup.prettify()

    clean_html = re.sub("-", "—", clean_html)
    f.write(clean_html)
    return clean_html

def getHtml(location):
    pageContent = open(location, 'r', encoding="utf-8").read()
    pageContent = clean_HTML(pageContent)

    tree = html.fromstring(pageContent)
    return clean_HTML(pageContent)

def getHtml_noEncoding(location):
    pageContent = open(location, 'r').read()
    pageContent = clean_HTML(pageContent)

    tree = html.fromstring(pageContent)
    return clean_HTML(pageContent)

def remove_duplicates(html):
    seen = set()
    new_html = ""
    for line in html.splitlines():
        if line not in seen:
            new_html += (line + "\n")
            seen.add(line)
    return new_html

# matching trees by tags
def simpleTreeMatching(Ts, Ti):
    roots = Ts.name
    rooti = Ti.name
    if roots == rooti:
        mchildren = Ts.findChildren(recursive = False)
        nchildren = Ti.findChildren(recursive = False)
        m = len(list(mchildren))
        n = len(list(nchildren))
        M = np.zeros((m + 1, n + 1))
        i,j = 1, 1
        for i in range(1, m+1):
            for j in range(1, n+1):
                Wij = simpleTreeMatching(mchildren[i-1], nchildren[j-1])
                M[i][j] = max([M[i][j-1], M[i-1][j], M[i-1][j-1] + Wij])
        M[m][n] += 1
        # if n > 0 and m > 0:
        #     return M[m][n]
        # # ce nima otrok primerjaj vsebino
        # else:
        #     if Ts.content == Ti.content:
        #         return M[m][n] + 1
        #     else:
        #         return M[m][n]
        # print(M[m][n], len(Ts.findChildren()), len(Ti.findChildren()))
        procentiVozlisc = M[m][n] / min(len(Ts.findChildren()), len(Ti.findChildren()))
        # print("Ujemanje v " + str(procentiVozlisc))
        if procentiVozlisc > 0.8:
            roadrunner(Ts.prettify(), Ti.prettify())
            #print(str(mchildren[i-1]).splitlines())
            #d = difflib.Differ()
            #diff = list(d.compare(str(mchildren[i-1]).splitlines(), str(nchildren[j-1]).splitlines()))
            #pprint.pprint(diff)
        return M[m][n]
    else:
        return 0

def roadrunner(html1, html2, output):
    # print((html1))
    lines1 = html1.splitlines()
    lines2 = html2.splitlines()

    d = difflib.Differ()
    diff_file = list(d.compare(lines1, lines2))
    # clean_HTML(open('WebPages/overstock.com/jewelry01.html', 'r'), "clean_html_1.html")
    # clean_HTML(open('WebPages/overstock.com/jewelry02.html', 'r'), "clean_html_2.html")
    # clean_HTML(open('WebPages/okusno.je/Sparglji - Okusno.je.html', 'r', encoding='utf-8'), "clean_html_1.html")
    # clean_HTML(open('WebPages/okusno.je/Testenine - Okusno.je.html', 'r', encoding='utf-8'), "clean_html_2.html")
    #clean_HTML(open('WebPages/rtvslo.si/Volvo XC 40 D4 AWD momentum_ suvereno med najboljše v razredu - RTVSLO.si.html', 'r', encoding='utf-8'), "clean_html_1.html")
    #clean_HTML(open('WebPages/rtvslo.si/Audi A6 50 TDI quattro_ nemir v premijskem razredu - RTVSLO.si.html', 'r', encoding='utf-8'), "clean_html_2.html")
    #with open('clean_html_1.html', 'r', encoding='utf-8') as file1:
        #with open('clean_html_2.html', 'r', encoding='utf-8') as file2:
    # html1 = file1.readlines()
    # html2 = file2.readlines()
    # d = difflib.Differ()
    # result = d.compare(html1, html2)
    # f = open("diff.txt", "w", encoding="utf-8")
    # f.write(''.join(result))

    #diff_file = open("diff.txt", "r", encoding="utf-8").readlines()
    diff = ""
    #print(diff_file)
    for line in diff_file:
        #print (line)
        if line[0] == "?": pass
        else: diff += (line + "\n")
    regex = r'((-\s*[^<>\n]+)\n)+((\+\s*[^<>\n]+)\n)+'
    try:
        deff_string = re.sub("((-\s*[^<>\n]+)\n)+((\+\s*[^<>\n]+)\n)+", "#text", diff)
        deff_string = re.sub("((\+\s*[^<>\n]+)\n)+((-\s*[^<>\n]+)\n)+", "#text", deff_string)
    except:
        deff_string = re.sub("((-\s*[^<>\n]+)\n)((\+\s*[^<>\n]+)\n)", "#text", diff)
        deff_string = re.sub("((\+\s*[^<>\n]+)\n)((-\s*[^<>\n]+)\n)", "#text", deff_string)
    deff_string = re.sub("-.*\n", "", deff_string)
    deff_string = re.sub("\+.*\n", "", deff_string)

    for i in range(0,5):
        deff_string = re.sub(r"(<[^\/\n]*?>)(\n\s*)(.*?)(\s+)(<\/.*?>){1}", r"'\g<1>\g<3>\g<5>'", deff_string, 0, re.MULTILINE)
        deff_string = remove_duplicates(deff_string)
    deff_string = re.sub(r"(')(\s*)(')", r"'\g<1>\g<3>'", deff_string, 0, re.MULTILINE)

    for i in range(0,8):
        deff_string = re.sub(r"(<[^\/\n]*>)(\s+)('.*')(\s*)(<\/.*>)\s", r"'\g<1>\g<3>\g<5>'\n", deff_string, 0, re.MULTILINE)
        deff_string = remove_duplicates(deff_string)
        deff_string = re.sub(r"(')(\s*)(')", r"'\g<1>\g<3>'", deff_string, 0, re.MULTILINE)

    deff_string = re.sub(r"(<[^\/\n]*>)(\s+)('.*')(\s*)(<\/.*>)\s", r"'\g<1>\g<3>\g<5>'\n", deff_string, 0, re.MULTILINE)
    deff_string = remove_duplicates(deff_string)

    deff_string = re.sub(r"'<", r"<", deff_string, 0, re.MULTILINE)

    deff_string = re.sub(r">'+<", r">\n<", deff_string, 0, re.MULTILINE)
    deff_string = re.sub("—", "-", deff_string)
    f = open(output, "w", encoding="utf-8")
    f.write(deff_string)

wrapper = ""
html1 = getHtml_noEncoding(webPages_overstock[0])
html2 = getHtml_noEncoding(webPages_overstock[1])
roadrunner(html1,html2, "overstock.txt")
# html1 = getHtml(webPages_rtv[0])
# html2 = getHtml(webPages_rtv[1])
roadrunner(html1,html2, "rtv.txt")
# html1 = getHtml(webPages_okusno[0])
# html2 = getHtml(webPages_okusno[1])
# simpleTreeMatching(BeautifulSoup(html1, 'html.parser'), BeautifulSoup(html2, 'html.parser'))
roadrunner(html1,html2, "okusno.txt")
