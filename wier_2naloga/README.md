#Ekstrakcija podatkov
##Knjiznice potrebne za zagon programa
numpy, beautifulsoup4, lxml, pytidylib, difflib
##Datoteke v repozitoriju
V mapi input-extraction/WebPages se nahajajo html datoteke, ki jih program za ekstrakcijo prebere. Iz vsake domene sta shranjeni po dve spletni strani, ki se nahajata v mapi z imenom domene.
V mapi implementation-extraction se nahaja programska koda. V datotekah A.py, B.py in C.py se nahajajo trije razlicni postopki, ki smo jih implementirali v okviru te naloge. Te algoritmi se kli�ejo iz datoteke run-extraction.py z argumentom, ki predstavlja tip algoritma, ki ga zelimo uporabiti za ekstrakcijo podatkov.

##Output
Za metodi A in B se izpisujejo json-i v kozolo
Za metodo C se wrapperji izpisujejo v rtv.txt, overstock.txt in okusno.txt